USE [INVISIONHELPDESK]
GO
/****** Object:  StoredProcedure [dbo].[E2_SP_NotifPanic]    Script Date: 3/17/2022 3:41:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[E2_SP_NotifPanic]
	@OrganizationID varchar(50)
	-- Add the parameters for the stored procedure here
	--@JenisKeterangan varchar(50), --'Release'
	--@LayerID varchar(50), --Default jika selain eskalasi silahkan diisi dengan 0
	--@TicketNumber varchar(50),
	--@UnitKerja varchar(50),
	--@UnitCase varchar(50),
	--@EmailAgent varchar(250),
	--@ReasonCodeID varchar(250) --ini untuk RFC By Reason Code
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--select * from E2_View_NotifPanic

	DECLARE @Username varchar(100), @SignalR varchar(200),@TicketNumber varchar(100), @TicketStatus VARCHAR(30), @SLA VARCHAR(50), 
	@UsedDaySLA Int,@WithinSLA VARCHAR(150),@UsedDaySLAOK VARCHAR(100),@ORGANIZATION varchar(10),
	@LoginCheck VARCHAR(50),@EmailID varchar(350),@EmailOutCorporate varchar(350),@EmailAddressTujuan varchar(150),@EmailSubject varchar(150)
	
	--select @EmailOutCorporate = outgoing_account_name from EmailAccounts where [Status]='Active'
	
	DECLARE @MessageOutput VARCHAR(100)
	Declare @EmailHTML varchar(MAX),@CheckKirimNotifTL varchar(350)
	--Send Email
	SELECT @EmailID= SUBSTRING(REPLACE(CONVERT(varchar, GETDATE(), 111), '/','') + REPLACE(CONVERT(varchar, GETDATE(), 108), ':',''),0,13)
	select @EmailOutCorporate = outgoing_account_name from EmailAccounts  where [Status]='Active' and rec_id=1
	
	--End

	DECLARE E2_View_NotifPanic_Cursor  CURSOR FOR 
	--Select b.ORGANIZATION as UnitCaseID,b.EMAIL_ADDRESS as EmailAddress,a.JenisKategori,a.JenisNotif from vw_EmailList_Release a left outer join msUser b on a.JenisRelated=b.LEVELUSER where a.JenisNotif=@JenisKeterangan and a.LayerID=@LayerID  and (b.NA='Y' and b.KIRIMEMAIL='YES')
	select msUser.Agent_SignalR,msUser.USERNAME,msUser.ORGANIZATION,
		TicketNumber,
		TicketStatus,
		SLA,
		UsedDaySLA,
		WithinSLA,
		'111' as UsedDaySLAOK from msUser left outer join E2_View_NotifPanic 
		on msUser.OUTBOUND=E2_View_NotifPanic.Outbound
				where  msUser.LEVELUSER='Agent' and msUser.OUTBOUND=1 and [LOGIN]=1
				--E2_View_NotifPanic.ORGANIZATION=@OrganizationID and
	OPEN E2_View_NotifPanic_Cursor 

	FETCH NEXT FROM E2_View_NotifPanic_Cursor INTO
		@SignalR,@Username,@ORGANIZATION,@TicketNumber, @TicketStatus, @SLA, @UsedDaySLA,@WithinSLA,@UsedDaySLAOK


	--Dynamic email Setup
	
	DECLARE @TempResultView TABLE (StatusResult varchar(100),IDSignalR varchar(500),Username varchar(150),ORGANIZATION varchar(150),TicketNumber varchar(150))
	
	--End
			--select * from msUser where LOGIN=1 and LEVELUSER='Supervisor'
			
			WHILE @@FETCH_STATUS = 0
			BEGIN
			--print '1'
				SELECT @LoginCheck = ORGANIZATION FROM msUser WHERE ORGANIZATION=@ORGANIZATION AND LOGIN=1 and LEVELUSER='Supervisor'
				SELECT @EmailAddressTujuan = EMAIL_ADDRESS from E2_View_EmailTL where ORGANIZATION=@ORGANIZATION
				SELECT @EmailSubject= '#'+ @TicketNumber +' '+ [Subject] from TR_TemplateNotifikasiEmail where [Type] ='Eskalasi'
				SELECT @EmailHTML= [Header_HTML] + [Body_HTML] + [Footer_HTML] from TR_TemplateNotifikasiEmail where [Type] ='Eskalasi'

				--Check kirim email apa sudah
				select @CheckKirimNotifTL = COUNT(JENIS_EMAIL) from ICC_EMAIL_OUT where TicketNumber=@TicketNumber and JENIS_EMAIL='EmailOverNotifTL'
				--End

				IF @UsedDaySLAOK = '111'
					BEGIN
						
								Print 'TRUE : ' + @TicketNumber
								insert into @TempResultView (StatusResult,IDSignalR,Username,ORGANIZATION,TicketNumber)
								select 'TRUE',@SignalR,@Username,@ORGANIZATION,@TicketNumber
								--End Email
								
										
						--IF @CheckKirimNotifTL = 0
						--	BEGIN
						--		insert into ICC_EMAIL_OUT (EMAIL_ID, DIRECTION, EFROM, ETO, ESUBJECT, JENIS_EMAIL, EBODY_HTML, Email_Date, TicketNumber, JENIS_EMAIL_INTERNAL, AGENT) VALUES (@EmailID,'out', @EmailOutCorporate, ''+ @EmailAddressTujuan +'', @EmailSubject, 'EmailOverNotifTL', @EmailHTML, GETDATE(), @TicketNumber, 'Email Notif Over SLA TL', 'System_SP_Notif')
						--	END
					END
				ELSE
					BEGIN
						insert into @TempResultView (StatusResult,IDSignalR,Username,ORGANIZATION,TicketNumber)
						select 'FALSE','xxxx','xxxx','xxxx','xxxx'
						--select 'TRUE',@SignalR,@Username,@ORGANIZATION,@TicketNumber
					END
			
				--RAISERROR(@MessageOutput,0,1) WITH NOWAIT

				FETCH NEXT FROM E2_View_NotifPanic_Cursor INTO
				@SignalR,@Username,@ORGANIZATION,@TicketNumber, @TicketStatus, @SLA, @UsedDaySLA,@WithinSLA,@UsedDaySLAOK
			END
			CLOSE E2_View_NotifPanic_Cursor
			DEALLOCATE E2_View_NotifPanic_Cursor
			select * from @TempResultView where StatusResult='TRUE' and IDSignalR <> '' and TicketNumber is not null
END

