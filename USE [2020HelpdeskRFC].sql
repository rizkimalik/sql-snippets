USE [2020HelpdeskRFC]
GO
/****** Object:  StoredProcedure [dbo].[SP_Temp_Survey_CSAT]    Script Date: 3/29/2022 9:37:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_Temp_Survey_CSAT]
	-- Add the parameters for the stored procedure here
	@JenisKeterangan varchar(50),
	--@LayerID varchar(50), --Default jika selain eskalasi silahkan diisi dengan 0
	@TicketNumber varchar(50)
	--,@UniqueID varchar(max) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
			Declare @UniqueID varchar(max)

			
			--Insert into Temp_Survey (Table Param untuk expired)
			DECLARE @ParamEndSurvey int;
			SET @ParamEndSurvey = 4;

			--Get UniqueID
			DECLARE @str VARCHAR(100) = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	
			;WITH E1(N) AS( -- 10 ^ 1 = 10 rows
				SELECT 1 FROM(VALUES (1),(1),(1),(1),(1),(1),(1),(1),(1),(1))t(N)
			),
			E2(N) AS(SELECT 1 FROM E1 a CROSS JOIN E1 b), -- 10 ^ 2 = 100 rows
			E4(N) AS(SELECT 1 FROM E2 a CROSS JOIN E2 b), -- 10 ^ 4 = 10,000 rows
			CteTally(N) AS(
				SELECT TOP(LEN(@str)) ROW_NUMBER() OVER(ORDER BY(SELECT NULL))
				FROM E4
			)
			SELECT @UniqueID =  (
				SELECT TOP(62)
					SUBSTRING(@str, N, 1)
				FROM CteTally t
				ORDER BY NEWID()
				FOR XML PATH('')
			) 

			--Cek jika dia mengisi 
			insert into Temp_Survey (UniqueID,TicketNumber,DateStart,DateEnd,Desc_Survey,Direction_Survey,HTML_Survey,Active_Survey) 
			values (@UniqueID,@TicketNumber,GETDATE(),DATEADD(day,@ParamEndSurvey,GETDATE()),'CSAT Module Email','OUT','','Y')
			
			-- tambahan malik CSAT autosolved
			insert into ICC_EMAIL_OUT (EMAIL_ID, DIRECTION, EFROM, ETO, ESUBJECT, EBODY_TEXT, EBODY_HTML, Email_Date, TicketNumber, JENIS_EMAIL_INTERNAL, AGENT) 
			VALUES (@EmailID,'out', @EmailOutCorporate, @EmailCustomer, @EmailSubject, 'Auto_Closed_3_Days_RFC_STATUS', @EmailHTML, GETDATE(), @TicketNumber, 'Email Notif Auto Closed STATUS Pending Information', 'System_SP')
			--End
			
	
END
