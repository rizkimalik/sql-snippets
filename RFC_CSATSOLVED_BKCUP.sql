USE [HelpdeskBKE]
GO
/****** Object:  StoredProcedure [dbo].[SP_RFC_CSATSolved]    Script Date: 3/31/2022 2:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_RFC_CSATSolved]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @EksekusiCreateCSAT VARCHAR(100), @TicketNumber VARCHAR(50), @JenisParam VARCHAR(50),@SendAutoClosed int,
	@EmailID VARCHAR(150),@EmailHTML varchar(max),@EmailSubject varchar(max),@DateCutOff varchar(100)

	set @DateCutOff = '2021-03-19 21:20:00'

	DECLARE @MessageOutput VARCHAR(100)

	select @EmailID= SUBSTRING(REPLACE(CONVERT(varchar, GETDATE(), 111), '/','') + REPLACE(CONVERT(varchar, GETDATE(), 108), ':',''),0,13)
	--select @EmailHTML= Body_HTML from TR_TemplateNotifikasiEmail where [Type] ='NotificationAutoClosed'
	select @EmailSubject= [Subject] from TR_TemplateNotifikasiEmail where [Type] ='CSAT'

	DECLARE vw_AutoClosed_Cursor CURSOR FOR 
    SELECT top 1000 CASE WHEN b.UniqueID <> '' THEN 'SudahDiCreate' ELSE 'BelumDiCreate' END EksekusiCreateCSAT, a.TicketNumber, 'Solved' as JenisParam 
	FROM tTicket a left outer join Temp_Survey b on a.TicketNumber=b.TicketNumber 
	left outer join Temp_Log_New_Helpdesk c on c.TicketNumber=a.TicketNumber where c.[LogType] ='AutoSolvedTicket' and a.UserClose='AutoSystem' 
	and b.UniqueID is null
	and a.DateCreate>@DateCutOff


	OPEN vw_AutoClosed_Cursor 

	FETCH NEXT FROM vw_AutoClosed_Cursor INTO
		@EksekusiCreateCSAT, @TicketNumber, @JenisParam

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @MessageOutput = @EksekusiCreateCSAT + ' ' + @TicketNumber + ' ' + @JenisParam
		--New Email Template System
		
		IF @EksekusiCreateCSAT = 'BelumDiCreate'
		   BEGIN
				exec SP_Temp_Survey_CSAT 'CSAT',@TicketNumber
				exec SP_Temp_Email_CSAT 'CSAT',@TicketNumber
		   END
		Else
			Begin
				SET @EksekusiCreateCSAT = 'Tidak_Create_Karena_Sudah_ada_CSAT_'+@TicketNumber
				PRINT @EksekusiCreateCSAT
			End
	   --PRINT @count
	   
	   
		--RAISERROR(@MessageOutput,0,1) WITH NOWAIT

		FETCH NEXT FROM vw_AutoClosed_Cursor INTO
		 @EksekusiCreateCSAT, @TicketNumber, @JenisParam
		 END
	END
	CLOSE vw_AutoClosed_Cursor
	DEALLOCATE vw_AutoClosed_Cursor
